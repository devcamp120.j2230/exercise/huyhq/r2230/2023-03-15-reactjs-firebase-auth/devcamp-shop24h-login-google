import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import ContentComponent from "./component/content/ContentComponent";
import FooterComponent from "./component/footer/FooterComponent";
import HeaderComponent from "./component/header/HeaderComponent";
import { useEffect } from 'react';
import { onAuthStateChanged } from 'firebase/auth';
import { useDispatch } from 'react-redux';
import auth from './firebase/config';

function App() {
  const dispatch = useDispatch();
  //lưu trữ thông tin đăng nhập
  useEffect(() => {
    onAuthStateChanged(auth, (result) => {
      dispatch({
        type: "LOGIN",
        data: result
      });
    })
  }, [])

  return (
    <div>
      <HeaderComponent />
      <ContentComponent />
      <FooterComponent />
    </div>
  );
}

export default App;
