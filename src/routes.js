import CartComponent from "./component/cart/CartComponent";
import HomeComponent from "./component/content/home/HomeComponent";
import LoginComponent from "./component/login/LoginComponent";
import ProductDetail from "./component/product/product-detail/ProductDetail";
import ProductList from "./component/product/ProductList";

const routeList = [
    { label: "", path: "/", element: <HomeComponent /> },
    { label: "Product", path: "/products", element: <ProductList /> },
    { label: "", path: "/products/:productId", element: <ProductDetail /> },
    { label: "", path: "/login", element: <LoginComponent /> },
    { label: "", path: "/cart", element: <CartComponent /> },
]

export default routeList;

