import { CART_GET_NUMBER_ITEM, CART_GET_SUBTOTAL } from "../constants/cart.constant"

export const getNumberItem = (num)=>{
    return {
        type: CART_GET_NUMBER_ITEM,
        data: num
    }
};

export const getSubtotalCart = (value)=>{
    return {
        type: CART_GET_SUBTOTAL,
        data: value
    }
};
