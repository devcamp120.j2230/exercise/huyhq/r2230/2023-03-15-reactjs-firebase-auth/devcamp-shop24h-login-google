import axios from "axios";
import { useSearchParams } from "react-router-dom";
import { PRODUCTS_FETCH_ERROR, PRODUCTS_FETCH_PENDING, PRODUCTS_FETCH_SUCCESS, PRODUCTS_PAGE_CHANGE, PRODUCT_FETCH_BY_ID_SUCCESS } from "../constants/product.constant";

const url = "http://localhost:8000/product"

const axiosCall = async (url, config) => {
    const response = await axios(url, config);
    return response.data.product;
};

export const getAllProduct = (limit, page, filter) => {
    return async (dispatch) => {
        await dispatch({
            type: PRODUCTS_FETCH_PENDING
        })

        var limitObj = ""

        if (filter && filter.name) {
            limitObj = { ...limitObj, name: filter.name };
        }

        if (filter && filter.min) {
            limitObj = { ...limitObj, min: filter.min };
        }

        if (filter && filter.max) {
            limitObj = { ...limitObj, max: filter.max };
        }

        var params = new URLSearchParams(limitObj);

        const response = await axios(url + "?" + params.toString());
        const lengthProduct = response.data.product.length;

        limitObj = { ...limitObj, start: (page - 1) * limit, limit };

        params = new URLSearchParams(limitObj);
        
        axiosCall(url + "?" + params.toString())
            .then(result => {
                return dispatch({
                    type: PRODUCTS_FETCH_SUCCESS,
                    length: lengthProduct,
                    data: result
                })
            })
            .catch(error => {
                return dispatch({
                    type: PRODUCTS_FETCH_ERROR,
                    error: error
                })
            });
    }
}

export const getProductById = (id)=>{
    return async (dispatch)=>{
        await dispatch({
            type: PRODUCTS_FETCH_PENDING
        })

        axiosCall(url + "/" + id)
        .then(result => {
            return dispatch({
                type: PRODUCT_FETCH_BY_ID_SUCCESS,
                data: result
            })
        })
        .catch(error => {
            return dispatch({
                type: PRODUCTS_FETCH_ERROR,
                error: error
            })
        });
        
    }
}
export const changePaginationAction = (page) => {
    return {
        type: PRODUCTS_PAGE_CHANGE,
        page
    }
    
}