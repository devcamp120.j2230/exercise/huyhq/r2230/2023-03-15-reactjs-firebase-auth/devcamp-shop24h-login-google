import { FILTER_PRODUCT_BY_CATEGORY, FILTER_PRODUCT_BY_MAX_PRICE, FILTER_PRODUCT_BY_MIN_PRICE, FILTER_PRODUCT_BY_NAME } from "../constants/filter.constant"

export const filterNameAction = (value)=>{
    return {
        type: FILTER_PRODUCT_BY_NAME,
        data: value
    }
}

export const filterMinPriceAction = (value)=>{
    return {
        type: FILTER_PRODUCT_BY_MIN_PRICE,
        data: value
    }
}

export const filterMaxPriceAction = (value)=>{
    return {
        type: FILTER_PRODUCT_BY_MAX_PRICE,
        data: value
    }
}

export const filterCategoryAction = (value)=>{
    return {
        type: FILTER_PRODUCT_BY_CATEGORY,
        data: value
    }
}
