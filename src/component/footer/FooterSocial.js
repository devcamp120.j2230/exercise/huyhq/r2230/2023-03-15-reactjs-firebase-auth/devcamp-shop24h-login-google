import { faFacebook, faInstagram, faTwitter, faYoutube } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Row } from "reactstrap";

const FooterSocial = () => {
    return (
        <Col>
            <Row className="justify-content-center">
                <Col md="6">
                    <h3 className="text-start">DevCamp</h3>
                </Col>
            </Row>
            <Row>
                <Col className="text-center">
                    <a href="#" className="me-3">
                        <FontAwesomeIcon icon={faFacebook} />
                    </a>
                    <a href="#" className="me-3">
                        <FontAwesomeIcon icon={faYoutube} />
                    </a>
                    <a href="#" className="me-3">
                        <FontAwesomeIcon icon={faInstagram} />
                    </a>
                    <a href="#" className="me-3">
                        <FontAwesomeIcon icon={faTwitter} />
                    </a>
                </Col>
            </Row>
        </Col>
    )
}

export default FooterSocial;