import { Row } from "reactstrap";
import FooterInfoProduct from "./FooterInfoProduct";
import FooterInfoService from "./FooterInfoService";
import FooterInfoSupport from "./FooterInfoSupport";
import FooterSocial from "./FooterSocial";

const FooterComponent = () => {
    return (
        <Row>
            <FooterInfoProduct/>
            <FooterInfoService/>
            <FooterInfoSupport/>
            <FooterSocial/>
        </Row>
    )
};

export default FooterComponent;