import { Grid, Typography, Button } from "@mui/material";


const ProductDetailDescription = (props) => {
    const product = props.productById;

    return (
        <Grid container justifyContent="center">
            <Grid item xs={12} flexDirection="column">
                <Typography component="div" variant="h5" fontWeight={700} my={3}>Description</Typography>
                <Typography component="div" variant="p" my={3}>{product.description}</Typography>
                <Grid
                    container
                    justifyContent="center"
                    maxHeight="150px"
                    sx={{
                        whiteSpace: "nowrap",
                        overflow: "hidden",
                        textOverflow: "ellipsis"
                    }}>
                    <Grid item xs={12} textAlign="center">
                        <Typography component="img" alt="Sample" src={product.imageUrl} maxWidth="100%" />
                    </Grid>
                </Grid>
                <Grid container justifyContent="center">
                    <Grid item xs={12} justifyContent="center" textAlign="center">
                        <Button
                            variant="contained"
                            sx={{
                                margin: "20px auto",
                                minWidth: "40px",
                                background: "black",
                                color: "white",
                            }}
                        >Xem thêm</Button>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>

    )
}

export default ProductDetailDescription;