import { Container, Grid } from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { getProductById } from "../../../action/product.action";
import BreadCrumb from "../../BreadCrumb";
import LastestProductsComponent from "../../content/home/LastestProductsComponent";
import ProductDetailDescription from "./ProductDetailDescription";
import ProductDetailInfo from "./ProductDetailInfo";

const breadcrumb = [
    {
        name: "Trang chủ",
        url: "/"
    },
    {
        name: "Danh mục sản phẩm",
        url: "/products"
    },
    {
        name: "Chi tiết",
        url: "#"
    },
]

const ProductDetail = () => {
    const { productId } = useParams();

    const dispatch = useDispatch()

    const { productById } = useSelector((reduxData) => {
        return reduxData.productReducer;
    })

    useEffect(() => {
        dispatch(getProductById(productId));
    }, []);

    return (
        <Container maxWidth={false} disableGutters>
            <Grid container justifyContent="center">
                <Grid item xs={12}>
                    <BreadCrumb link={JSON.stringify(breadcrumb)} />
                </Grid>
                <Grid item xs={12} >
                    <ProductDetailInfo productById={productById} />
                </Grid>
                <Grid item xs={12} flexDirection="column" >
                    <ProductDetailDescription productById={productById} />
                </Grid>
                <Grid item xs={12} flexDirection="column" >
                    <LastestProductsComponent />
                </Grid>
            </Grid>
        </Container >
    )
}

export default ProductDetail;