import { Grid, Typography, Button } from "@mui/material";

const ProductDetailInfo = (props) => {
    const product = props.productById;

    const formatNumber = (price) => {
        return new Intl.NumberFormat().format(price)
    }

    return (
        <Grid container justifyContent="center">
            <Grid item xs={4} md={4} sm={12} flexDirection="column" textAlign="center">
                <Typography component="img" alt="Sample" src={product.imageUrl} maxWidth="100%" />
                <Grid container flexDirection="row" justifyContent="center">
                    <Grid item xs={4} p={3}>
                        <Typography component="img" alt="Sample" src={product.imageUrl} maxWidth="100%" />
                    </Grid>
                    <Grid item xs={4} p={3}>
                        <Typography component="img" alt="Sample" src={product.imageUrl} maxWidth="100%" />
                    </Grid>
                    <Grid item xs={4} p={3}>
                        <Typography component="img" alt="Sample" src={product.imageUrl} maxWidth="100%" />
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={8} md={8} sm={12} flexDirection="column" p={10}>
                <Typography variant="h4" component="div" fontWeight={700} my={3}>{product.name}</Typography>
                <Typography variant="p" component="div" color="text.secondary" my={3}>Brand: {product.type}</Typography>
                <Typography variant="p" component="div" color="text.secondary" my={3}>Rated: </Typography>
                <Typography variant="p" component="div" color="text.primary" my={3}>{product.description}</Typography>
                <Typography variant="h5" component="div" fontWeight={700} color="red" my={3}>{formatNumber(product.buyPrice)}</Typography>
                <Grid container flexDirection="row" my={3}>
                    <Button
                        variant="contained"
                        width="40px"
                        sx={{
                            margin: "0 10px",
                            minWidth: "20px",
                            minHeight: "20px",
                            background: "grey",
                            borderRadius: "50%"
                        }}
                    >-</Button>
                    <Typography component="div">
                        <Typography variant="span" component="div" color="text.primary" mt={1}>1</Typography>
                    </Typography>
                    <Button
                        variant="contained"
                        width="40px"
                        sx={{
                            margin: "0 10px",
                            minWidth: "20px",
                            minHeight: "20px",
                            background: "grey",
                            borderRadius: "50%"
                        }}
                    >+</Button>
                </Grid>
                <Button
                    variant="contained"
                    sx={{
                        margin: "20px 0px",
                        minWidth: "40px",
                        background: "black",
                        color: "white",
                    }}
                >Add to card</Button>
            </Grid>
        </Grid>
    )
}

export default ProductDetailInfo;