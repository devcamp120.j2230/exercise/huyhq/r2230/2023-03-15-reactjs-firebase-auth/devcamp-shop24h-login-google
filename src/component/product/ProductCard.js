import { Grid, Card, CardContent, Typography, Button } from "@mui/material";
import { useLocation } from "react-router-dom";

const ProductCard = (props) => {
    const product = JSON.parse(props.product);
    const location = useLocation();
    const formatNumber = (price) => {
        return new Intl.NumberFormat().format(price)
    }

    return (
        <Grid item xs={3} lg={3} md={6} sm={12} justifyContent="center" p={3}>
            <Card sx={{ minWidth: 275 }}>
                <CardContent>
                    <Typography gutterBottom sx={{ maxWidth: "100%" }}>
                        <Typography component="img" alt="Sample" src={product.imageUrl} maxWidth="100%" />
                    </Typography>
                    <Typography component="a" href={"/products/" + product._id} >
                        <Typography
                            variant="h5"
                            component="div"
                            textAlign="center"
                            minHeight={40}
                            sx={{
                                whiteSpace: "nowrap",
                                overflow: "hidden",
                                textOverflow: "ellipsis"
                            }}>
                            {product.name}
                        </Typography>
                        <Typography component="div" textAlign="center">
                            <Typography variant="span" mr={3} sx={{ fontSize: "0.8rem", textDecoration: "line-through" }} color="text.secondary">
                                {formatNumber(product.buyPrice)}
                            </Typography>
                            <Typography variant="span" color="red">
                                {formatNumber(product.promotionPrice)}
                            </Typography>
                        </Typography>
                    </Typography>
                </CardContent>
            </Card>
        </Grid>
    )
}

export default ProductCard;