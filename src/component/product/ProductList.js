import { Grid, Pagination, Stack } from "@mui/material";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import { changePaginationAction, getAllProduct } from "../../action/product.action";
import BreadCrumb from "../BreadCrumb";
import ProductCard from "./ProductCard";
import ProductFilters from "./ProductFilters";
const breadcrumb = [
    {
        name: "Trang chủ",
        url: "/"
    },
    {
        name: "Danh mục sản phẩm",
        url: "#"
    }
]

const ProductList = () => {
    const dispatch = useDispatch();
    const location = useLocation();
    const { products, pending, currentPage, countPage, limitProduct } = useSelector((reduxData) => {
        return reduxData.productReducer;
    })
    useEffect(() => {
        dispatch(getAllProduct(limitProduct, currentPage));
    }, [currentPage])

    const onChangePagination = (event, value) => {
        dispatch(changePaginationAction(value));
    }
    return (
        <Grid container>
            <Grid item xs={12}>
                <BreadCrumb link={JSON.stringify(breadcrumb)} />
            </Grid>
            <Grid item xs={2}>
                <ProductFilters />
            </Grid>
            <Grid item xs={10}>
                <Grid container>
                    <Grid item xs={12}>
                        <Grid container>
                            {
                                products !== ""
                                    ? products.map((product, i) => {
                                        return <React.Fragment key={i}>
                                            <ProductCard product={JSON.stringify(product)} />
                                        </React.Fragment>
                                    })
                                    : <></>
                            }
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <Stack spacing={2} sx={{ float: "right" }}>
                            <Pagination count={countPage} page={currentPage} onChange={onChangePagination} />
                        </Stack>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default ProductList;