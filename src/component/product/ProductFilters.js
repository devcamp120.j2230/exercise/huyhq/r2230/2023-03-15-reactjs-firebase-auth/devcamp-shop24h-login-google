import { faStar as StarReg } from "@fortawesome/free-regular-svg-icons";
import { faStar as StarSolid } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Grid, Paper, MenuList, MenuItem, Typography, FormGroup, FormControlLabel, Checkbox, TextField, Button } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { useSearchParams } from "react-router-dom";
import { filterCategoryAction, filterMaxPriceAction, filterMinPriceAction, filterNameAction } from "../../action/filterProduct.action";
import { getAllProduct } from "../../action/product.action";

const categoriesCheckbox = [
    { label: "TV", name: "Tivi", check: false },
    { label: "Tủ lạnh", name: "Tủ lạnh", check: false },
    { label: "Máy tình bàn", name: "Tủ lạnh", check: false },
    { label: "Điện thoại", name: "Điện thoại", check: false },
    { label: "Máy tính bảng", name: "Tablet", check: false },
]
const ProductFilters = () => {
    const [searchParams, setSearchParams] = useSearchParams();
    const dispatch = useDispatch();

    const { limitProduct, currentPage } = useSelector((reduxData) => {
        return reduxData.productReducer;
    })

    const { nameFilter, minPriceFilter, maxPriceFilter, categoryFilter } = useSelector((reduxData) => {
        return reduxData.filterReducer;
    });

    const onInputNameFilter = (e) => {
        dispatch(filterNameAction(e.target.value));
    }

    const onInputMinPriceFilter = (e) => {
        dispatch(filterMinPriceAction(e.target.value));
    }

    const onInputMaxPriceFilter = (e) => {
        dispatch(filterMaxPriceAction(e.target.value));
    }

    const onCategoryCheckbox = (e) => {
        var key = e.target.value;
        var newCat = categoriesCheckbox;
        newCat[key].check = e.target.checked
        dispatch(filterCategoryAction(e.target.value));
    }

    const onClickButtonFilter = () => {
        var filter = ""

        if (nameFilter !== "") {
            filter = { ...filter, name: nameFilter }
        }

        if (minPriceFilter !== 0) {
            filter = { ...filter, min: minPriceFilter }
        }

        if (maxPriceFilter !== Number.MAX_VALUE) {
            filter = { ...filter, max: maxPriceFilter }
        }

        setSearchParams(filter);
        
        dispatch(getAllProduct(limitProduct, currentPage, filter));
    }

    return (
        <Grid item flexDirection="column">
            <Grid container my={5}>
                <Grid item flexDirection="column">
                    <Typography component="div" variant="h5" pl={2} fontWeight={700}>Products</Typography>
                    <Paper sx={{ border: "none", boxShadow: "none" }}>
                        <MenuList sx={{ border: "none", boxShadow: "none" }}>
                            <MenuItem>
                                <TextField
                                    id="outlined"
                                    placeholder="Name"
                                    onChange={onInputNameFilter}
                                />
                            </MenuItem>
                        </MenuList>
                    </Paper>
                </Grid>
            </Grid>
            <Grid container my={5}>
                <Grid item flexDirection="column">
                    <Typography component="div" variant="h5" pl={2} fontWeight={700}>Price</Typography>
                    <Grid container my={1}>
                        <Grid item xs={5} px={1}>
                            <TextField
                                id="outlined"
                                placeholder="min"
                                onChange={onInputMinPriceFilter}
                            />
                        </Grid>
                        <Grid item xs={2} textAlign="center" lineHeight="3">
                            <Typography component="span">-</Typography>
                        </Grid>
                        <Grid item xs={5} px={1}>
                            <TextField
                                id="outlined"
                                placeholder="max"
                                onChange={onInputMaxPriceFilter}
                            />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Grid container my={5}>
                <Grid item flexDirection="column">
                    <Typography component="div" variant="h5" pl={2} fontWeight={700}>Categories</Typography>

                    <FormGroup sx={{ paddingLeft: "16px" }}>
                        {
                            categoriesCheckbox.map((category, i) => {
                                return <FormControlLabel
                                    key={i}
                                    control={
                                        <Checkbox
                                            value={i}
                                            name={category.name}
                                            onChange={onCategoryCheckbox}
                                        />
                                    }
                                    labelPlacement="end"
                                    label={category.label} />
                            })
                        }
                    </FormGroup>
                </Grid>
            </Grid>
            <Grid container my={5}>
                <Grid item flexDirection="column">
                    <Typography component="div" variant="h5" pl={2} fontWeight={700}>Rating</Typography>
                    <FormGroup sx={{ paddingLeft: "16px" }}>
                        <FormControlLabel control={<Checkbox defaultChecked />} labelPlacement="end"
                            label={
                                <>
                                    <FontAwesomeIcon icon={StarSolid} color="orange" />
                                    <FontAwesomeIcon icon={StarSolid} color="orange" />
                                    <FontAwesomeIcon icon={StarSolid} color="orange" />
                                    <FontAwesomeIcon icon={StarSolid} color="orange" />
                                    <FontAwesomeIcon icon={StarSolid} color="orange" />
                                </>
                            } />
                        <FormControlLabel control={<Checkbox />} labelPlacement="end"
                            label={
                                <>
                                    <FontAwesomeIcon icon={StarSolid} color="orange" />
                                    <FontAwesomeIcon icon={StarSolid} color="orange" />
                                    <FontAwesomeIcon icon={StarSolid} color="orange" />
                                    <FontAwesomeIcon icon={StarSolid} color="orange" />
                                    <FontAwesomeIcon icon={StarReg} color="gray" />
                                </>
                            } />
                        <FormControlLabel control={<Checkbox />} labelPlacement="end"
                            label={
                                <>
                                    <FontAwesomeIcon icon={StarSolid} color="orange" />
                                    <FontAwesomeIcon icon={StarSolid} color="orange" />
                                    <FontAwesomeIcon icon={StarSolid} color="orange" />
                                    <FontAwesomeIcon icon={StarReg} color="gray" />
                                    <FontAwesomeIcon icon={StarReg} color="gray" />
                                </>
                            } />
                        <FormControlLabel control={<Checkbox />} labelPlacement="end"
                            label={
                                <>
                                    <FontAwesomeIcon icon={StarSolid} color="orange" />
                                    <FontAwesomeIcon icon={StarSolid} color="orange" />
                                    <FontAwesomeIcon icon={StarReg} color="gray" />
                                    <FontAwesomeIcon icon={StarReg} color="gray" />
                                    <FontAwesomeIcon icon={StarReg} color="gray" />
                                </>
                            } />
                        <FormControlLabel control={<Checkbox />} labelPlacement="end"
                            label={
                                <>
                                    <FontAwesomeIcon icon={StarSolid} color="orange" />
                                    <FontAwesomeIcon icon={StarReg} color="gray" />
                                    <FontAwesomeIcon icon={StarReg} color="gray" />
                                    <FontAwesomeIcon icon={StarReg} color="gray" />
                                    <FontAwesomeIcon icon={StarReg} color="gray" />
                                </>
                            } />
                    </FormGroup>
                </Grid>
            </Grid>
            <Grid container my={5}>
                <Button variant="contained" color="primary" sx={{ marginLeft: "16px" }} onClick={onClickButtonFilter}>Lọc</Button>
            </Grid>
        </Grid>
    )
}

export default ProductFilters;