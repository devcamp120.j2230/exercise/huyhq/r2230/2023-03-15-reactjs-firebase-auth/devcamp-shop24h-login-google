import { faGoogle } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Container, Grid, Button, TextField, Typography } from "@mui/material";

import { signInWithPopup, GoogleAuthProvider, signOut, onAuthStateChanged } from 'firebase/auth';
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import auth from "../../firebase/config";
const provider = new GoogleAuthProvider();

const LoginComponent = () => {
    const navigate = useNavigate();					//chuyển hướng sang route khác
    const dispatch = useDispatch();

    const { user } = useSelector((reduxData) => {
        return reduxData.userReducer;
    });

    //chức năng đăng nhập
    const logInGoogle = () => {
        signInWithPopup(auth, provider)
            .then((result) => {
                dispatch({
                    type: "LOGIN",
                    data: result.user
                });
            })
            .catch((error) => {
                console.error(error);
            })
    }

    //chức năng log out
    const logOutGoogle = () => {
        signOut(auth)
            .then(() => {
                dispatch({
                    type: "LOGOUT",
                });
            })
            .catch((error) => {
                console.error(error);
            })
    }

    //lưu trữ thông tin đăng nhập
    useEffect(() => { 
        if (user) {
          navigate("/");
        }
    }, [user])

    return (
        <Container maxWidth="medium">
            {
                user !== null
                    ? <div>
                        <h1>Hello {user.displayName}</h1>
                        <Button variant="contained" onClick={logOutGoogle}>Log Out</Button>
                    </div>
                    : <Grid container justifyContent="center" py="100px">
                        <Grid
                            item xs={4} md={8} sm={12} lg={4}
                            sx={{ background: "#DFE7E9" }}
                            textAlign="center"
                            justifyContent="center"
                            p="20px"
                        >
                            <Grid container
                                sx={{ background: "#ffff" }}
                            >
                                <Grid item xs={12} my={3} px={3}>
                                    <Button
                                        variant="contained"
                                        gap={2}
                                        sx={{
                                            minWidth: "300px",
                                            color: "#ffff",
                                            background: "#DE4830",
                                            borderRadius: "20px"
                                        }}
                                        onClick={logInGoogle}
                                    >
                                        <Typography variant="p" component="div" mx={1}>
                                            <FontAwesomeIcon icon={faGoogle} />
                                        </Typography>
                                        <Typography variant="p" component="div" mx={1}>Sigin with Google</Typography>
                                    </Button>
                                </Grid>
                                <Grid item xs={12}>
                                    <hr />
                                </Grid>
                                <Grid item xs={12} my={3} px={3}>
                                    <TextField placeholder="Username" fullWidth sx={{ borderRadius: "20px" }} />
                                </Grid>
                                <Grid item xs={12} my={3} px={3}>
                                    <TextField placeholder="Password" fullWidth sx={{ borderRadius: "20px" }} />
                                </Grid>
                                <Grid item xs={12} my={3} px={3}>
                                    <Button
                                        variant="contained"
                                        sx={{
                                            minWidth: "300px",
                                            color: "#ffff",
                                            background: "#29A744",
                                            borderRadius: "20px"
                                        }}
                                    >
                                        Sigin
                                    </Button>
                                </Grid>
                            </Grid>

                        </Grid>
                    </Grid>
            }

        </Container>
    )
}

export default LoginComponent;