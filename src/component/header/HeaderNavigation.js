import { Container, Grid, Toolbar, Button, Box } from "@mui/material";
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';

import routeList from "../../routes";
import { useState } from "react";

const HeaderNavigation = () => {

    return (
        <Grid item xs={8}>
            <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
                {routeList.map((route, index) => (
                    route.label != ""
                        ? <Button
                            key={index}
                            color="primary"
                            sx={{ my: 2, display: 'block' }}
                            href={route.path}
                        >
                            {route.label}
                        </Button>
                        : null
                ))}
            </Box>
        </Grid>
    )
}

export default HeaderNavigation;