import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBell, faCartShopping, faUser } from "@fortawesome/free-solid-svg-icons";

import { Grid, Menu, MenuItem, Divider, Typography } from "@mui/material";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import auth from "../../firebase/config";
import { signOut } from "firebase/auth";

const HeaderIconNavBar = () => {
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const dispatch = useDispatch();

    const { user } = useSelector((reduxData) => {
        return reduxData.userReducer
    })

    const { itemNum } = useSelector((reduxData)=>{
        return reduxData.cartReducer;
    })
    //chức năng log out
    const logOutGoogle = () => {
        signOut(auth)
            .then(() => {
                dispatch({
                    type: "LOGOUT",
                });
            })
            .catch((error) => {
                console.error(error);
            })
    }
    return (
        <Grid item xs={2}>
            <Grid container justifyContent="center" alignItems="center" lineHeight={3}>
                <Grid item>
                    <a href="#" className="header-icon me-3">
                        <FontAwesomeIcon icon={faBell} />
                    </a>
                </Grid>
                <Grid item>
                    <Typography
                        component="a"
                        mr={2}
                        aria-label="more"
                        id="long-button"
                        aria-controls={open ? 'long-menu' : undefined}
                        aria-expanded={open ? 'true' : undefined}
                        aria-haspopup="true"
                        sx={{ cursor: "pointer" }}
                        onClick={handleClick}
                    >
                        {
                            user
                                ? <Typography component="img" src={user.photoURL} sx={{ width: "30px", borderRadius: "50%" }} />
                                : <FontAwesomeIcon icon={faUser} />
                        }

                    </Typography>
                    <Menu
                        id="long-menu"
                        MenuListProps={{
                            'aria-labelledby': 'long-button',
                        }}
                        anchorEl={anchorEl}
                        open={open}
                        onClose={handleClose}
                        PaperProps={{
                            style: {
                                maxHeight: '100px',
                                width: 'auto',
                            },
                        }}
                    >
                        {
                            user
                                ? <>
                                    <Typography component="div" color="text.secondary" mx={2}>
                                        {user.displayName}
                                    </Typography>
                                    <MenuItem selected={false} onClick={handleClose}>
                                        <Typography component="a" onClick={logOutGoogle} href="/">Logout</Typography>
                                    </MenuItem>
                                </>

                                : <MenuItem selected={false} onClick={handleClose}>
                                    <Typography component="a" href="/login">Login</Typography>
                                </MenuItem>
                        }


                    </Menu>

                </Grid>
                <Grid item>
                    <a href="/cart" className="header-icon me-3">
                        <FontAwesomeIcon icon={faCartShopping} />
                        <Typography
                            component="span"
                            variant="span"
                            textAlign="center"
                            sx={{
                                position: "relative",
                                top: "5px",
                                left: "-5px",
                                padding: "1px 5px",
                                fontSize: "12px",
                                background: "gray",
                                color: "white",
                                borderRadius: "50%"
                            }}
                        >
                            {itemNum}
                        </Typography>
                    </a>
                </Grid>
            </Grid>
        </Grid>
    )
};

export default HeaderIconNavBar;