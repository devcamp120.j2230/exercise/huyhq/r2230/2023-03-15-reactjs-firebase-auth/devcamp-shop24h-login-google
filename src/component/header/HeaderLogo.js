import { Grid, Typography } from "@mui/material";

const HeaderLogo = () => {
    return (
        <Grid item xs={2} textAlign="center">
            <Typography variant="h3" component="a" href="/" sx={{cursor: "pointer"}}>DevCamp</Typography>
        </Grid>
    );
};

export default HeaderLogo;