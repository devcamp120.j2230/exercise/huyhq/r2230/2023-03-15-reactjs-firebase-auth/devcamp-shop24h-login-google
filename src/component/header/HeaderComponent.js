import { Container, Grid } from "@mui/material";
import { Col, Row } from "reactstrap";
import HeaderIconNavBar from "./HeaderIconNavBar";
import HeaderLogo from "./HeaderLogo";
import HeaderNavigation from "./HeaderNavigation";
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css"></link>
const HeaderComponent = () => {
    return (
        <Container maxWidth={false} disableGutters>
            <Grid container my={5}>
                <HeaderLogo />
                <HeaderNavigation />
                <HeaderIconNavBar />
            </Grid>
        </Container>
    );
}

export default HeaderComponent;