import { Typography, Breadcrumbs, Link } from "@mui/material";

const BreadCrumb = (props) => {
    const links = JSON.parse(props.link);
    return (
        <Breadcrumbs separator="››" aria-label="breadcrumb">
            {
                links.map((link, i) => {
                    return <Link underline="hover" color="text.dark" href={link.url} key={i}>
                                {link.name}
                            </Link>
                })
            }

            {/* <Typography color="text.primary" href="#">Product</Typography> */}
        </Breadcrumbs>
    )
}

export default BreadCrumb;