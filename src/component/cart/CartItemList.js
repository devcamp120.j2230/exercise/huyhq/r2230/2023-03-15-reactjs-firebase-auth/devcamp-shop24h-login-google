import { TableContainer, TableHead, TableRow, TableCell, TableBody, Paper, Table } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getSubtotalCart } from "../../action/cart.action";
import CartItem from "./CartItem";

const CartItemList = () => {
    const dispatch = useDispatch();

    const { subtotal, cart } = useSelector((reduxData)=>{
        return reduxData.cartReducer;
    })

    useEffect(() => {
        var total = 0;
        cart.map((item, i) => {
            total = total + item.price * item.quantity
        });
        dispatch(getSubtotalCart(total));
    }, [])

    return (
        <TableContainer component={Paper} sx={{ boxShadow: "none" }}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead sx={{ border: "none" }}>
                    <TableRow sx={{ border: "none" }}>
                        <TableCell align="left" sx={{ fontWeight: 700 }}>Name</TableCell>
                        <TableCell align="left" sx={{ fontWeight: 700 }}>Price</TableCell>
                        <TableCell align="left" sx={{ fontWeight: 700 }}>Quantity</TableCell>
                        <TableCell align="left" sx={{ fontWeight: 700 }}>Total</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        cart.map((item, i) => {
                            return <React.Fragment key={i}>
                                <CartItem item={item}/>
                            </React.Fragment>
                        })
                    }
                </TableBody>
            </Table>
        </TableContainer >
    )
}
export default CartItemList