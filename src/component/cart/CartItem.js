import { TableRow, TableCell, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getSubtotalCart } from "../../action/cart.action";

const CartItem = (props) => {
    const dispatch = useDispatch();

    const { subtotal } = useSelector((reduxData)=>{
        return reduxData.cartReducer;
    })

    const item = props.item;    

    const [quantity, setQuantity] = useState(item.quantity);

    const [price, setPrice] = useState(item.price);

    const [total, setTotal] = useState(price * quantity);

    const onClickButtonIncrease = () => {
        var val = quantity;
        val = val + 1;
        console.log(subtotal);
        setQuantity(val);
        dispatch(getSubtotalCart(subtotal + price))
    }

    const onClickButtonDicrease = () => {
        var val = quantity;
        val = val - 1;
        console.log(subtotal);
        setQuantity(val);
        dispatch(getSubtotalCart(subtotal - price))
    }

    useEffect(() => {
        setTotal(price * quantity);
    }, [quantity])

    return (
        <TableRow
            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
        >
            <TableCell scope="row" align="left">
                <Typography component="img" maxWidth="80px" mr={1} src="https://www.shorturl.at/img/shorturl-icon.png" />
                <Typography component="span">{item.name}</Typography>
            </TableCell>
            <TableCell align="left">${price}</TableCell>
            <TableCell align="left">
                <Typography
                    component="button"
                    p={1}
                    sx={{ border: "none", minWidth: "40px" }}
                    onClick={onClickButtonDicrease}

                >
                    -
                </Typography>
                <Typography component="button" p={1} sx={{ border: "none", minWidth: "40px" }}>{quantity}</Typography>
                <Typography
                    component="button"
                    p={1}
                    sx={{ border: "none", minWidth: "40px" }}
                    onClick={onClickButtonIncrease}
                >
                    +
                </Typography>
            </TableCell>
            <TableCell align="left">${total}</TableCell>
        </TableRow>
    )
}

export default CartItem