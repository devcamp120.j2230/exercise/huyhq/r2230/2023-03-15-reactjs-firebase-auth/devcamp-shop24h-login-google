import { Grid, Typography, Button, TextField } from "@mui/material";
import { Container } from "@mui/system"
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import "../../App.css"
import { useNavigate } from "react-router-dom";
import CartItemList from "./CartItemList";

const CartComponent = () => {
    const navigate = useNavigate();
    
    const { subtotal, discount, total } = useSelector((reduxData)=>{
        return reduxData.cartReducer;
    })
    
    return (
        <Container>
            <Grid container justifyContent="center" flexDirection="column">
                <Grid item my={3}>
                    <CartItemList />
                </Grid>
                <Grid item my={3}>
                    <Typography component="button" sx={{ border: "none" }} p={1} onClick={()=>{navigate(-1)}}>Continue Shopping</Typography>
                </Grid>
                <Grid item my={3}>
                    <Grid container>
                        <Grid item xs={6}>
                            <Grid container>
                                <Grid item xs={12} my={3}>
                                    <Typography component="div" variant="p" fontWeight={700}>Discount code</Typography>
                                </Grid>
                                <Grid item xs={6} pr={3}>
                                    <TextField placeholder="Nhập mã"></TextField>
                                </Grid>
                                <Grid item xs={6} pt={1}>
                                    <Button variant="contained" sx={{ background: "gray" }}>Thêm</Button>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={6}>
                            <Grid container alignContent="center" flexDirection="column" bgcolor="#F0F0F0" px={10} py={3}>
                                <Typography component="div" variant="p" fontWeight={700}>Cart total</Typography>
                                <Grid container my={2}>
                                    <Grid item xs={8} >
                                        <Typography component="div" variant="p" fontWeight={700}>Subtotal</Typography>
                                    </Grid>
                                    <Grid item xs={4} textAlign="right">
                                        <Typography component="div" variant="p" fontWeight={700} color="error.main">${subtotal}</Typography>
                                    </Grid>
                                </Grid>
                                <Grid container my={2}>
                                    <Grid item xs={8} >
                                        <Typography component="div" variant="p" fontWeight={700}>Discount</Typography>
                                    </Grid>
                                    <Grid item xs={4} textAlign="right">
                                        <Typography component="div" variant="p" fontWeight={700} color="error.main">${discount}</Typography>
                                    </Grid>
                                </Grid>
                                <Grid container my={2}>
                                    <Grid item xs={8} >
                                        <Typography component="div" variant="p" fontWeight={700}>Total</Typography>
                                    </Grid>
                                    <Grid item xs={4} textAlign="right">
                                        <Typography component="div" variant="p" fontWeight={700} color="error.main">${total}</Typography>
                                    </Grid>
                                </Grid>
                                <Button variant="contained" sx={{ background: "#7FAC38" }}>Checkout</Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Container>
    )
}
export default CartComponent;