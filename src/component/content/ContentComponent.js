
import { Routes, Route } from "react-router-dom";
import { Col } from "reactstrap";
import routeList from "../../routes";
import HomeComponent from "./home/HomeComponent";



const ContentComponent = () => {
    return (
        <Col className="m-5">
            <Routes>
                {
                    routeList.map((route, index) => {
                        if (route.path) {
                            return <Route path={route.path} element={route.element} key={index}></Route>
                        } else {
                            return null
                        }
                    })
                }
                <Route path="*" element={<HomeComponent />}></Route>

            </Routes>
        </Col>
    );
}

export default ContentComponent;