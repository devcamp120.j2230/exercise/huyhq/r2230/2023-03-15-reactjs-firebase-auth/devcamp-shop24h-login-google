import { Grid, Typography } from "@mui/material";
import { Container } from "@mui/system";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllProduct } from "../../../action/product.action";
import ProductCard from "../../product/ProductCard";

const LastestProductsComponent = () => {
    const limitProduct = 8;
    const { products, pending, currentPage } = useSelector((reduxData) => {
        return reduxData.productReducer;
    })

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getAllProduct(limitProduct, currentPage));
    }, [])

    const formatNumber = (price) => {
        return new Intl.NumberFormat().format(price)
    }
    return (
        <Container maxWidth={false} disableGutters>
            <Typography variant="h5" component="div" fontWeight={700} my={3}>Latest Product</Typography>
            <Grid container justifyContent="center">
                {
                    products !== ""
                        ? products.map((product, i) => {
                            return <React.Fragment key={i}>
                                <ProductCard product={JSON.stringify(product)} />
                            </React.Fragment>
                        })
                        : <></>
                }
            </Grid>
        </Container>
    )
}

export default LastestProductsComponent;