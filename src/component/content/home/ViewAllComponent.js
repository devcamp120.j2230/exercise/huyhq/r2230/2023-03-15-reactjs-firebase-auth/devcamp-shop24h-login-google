import { Col, Button } from "reactstrap";

const ViewAllComponent = ()=>{
    return(
        <Col className="text-center">
            <Button color="primary">View All</Button>
        </Col>
    )
}

export default ViewAllComponent;