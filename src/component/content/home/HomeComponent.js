import CarouselComponent from "./CarouselComponent"
import LastestProductsComponent from "./LastestProductsComponent"
import ViewAllComponent from "./ViewAllComponent"

const HomeComponent = () => {
    return (
        <>
            <CarouselComponent />
            <LastestProductsComponent />
            <ViewAllComponent />
        </>
    )
}

export default HomeComponent;