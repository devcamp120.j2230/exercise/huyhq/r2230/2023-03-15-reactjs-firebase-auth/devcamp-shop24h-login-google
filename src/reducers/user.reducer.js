const userState = {
    user: null
}

const userReducer = (state = userState, action) => {
    switch (action.type) {
        case "LOGIN":
            state.user = action.data;
            break;
        case "LOGOUT":
            state.user = null;
            break;
        default:
            break;
    }
    return { ...state };
};

export default userReducer;