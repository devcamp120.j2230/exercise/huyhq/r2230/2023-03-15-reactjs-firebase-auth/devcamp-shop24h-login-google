import { CART_GET_NUMBER_ITEM, CART_GET_SUBTOTAL } from "../constants/cart.constant";
import { cartItem } from "../data/cart.data";

const cartState = {
    cart: cartItem,
    itemNum: 0,
    subtotal: 0,
    discount: 0,
    total: 0,
}

const cartReducer = (state = cartState, action)=>{
    switch (action.type) {
        case CART_GET_SUBTOTAL:
            state.subtotal = action.data;
            break;
    
        default:
            break;
    }
    state.itemNum = state.cart.length;
    state.total = state.subtotal - state.discount;
    return {...state}
}

export default cartReducer;
 