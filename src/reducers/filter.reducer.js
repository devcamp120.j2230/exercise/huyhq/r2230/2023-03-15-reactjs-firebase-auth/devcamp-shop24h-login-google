import { FILTER_PRODUCT_BY_CATEGORY, FILTER_PRODUCT_BY_MAX_PRICE, FILTER_PRODUCT_BY_MIN_PRICE, FILTER_PRODUCT_BY_NAME } from "../constants/filter.constant";

const filterState = {
    nameFilter: "",
    minPriceFilter: 0,
    maxPriceFilter: Number.MAX_VALUE,
    categoryFilter: [],
    currentPageFilter: 0,
    limitProductFilter: 6,
}

const filterReducer = (state = filterState, action) => {
    switch (action.type) {
        case FILTER_PRODUCT_BY_NAME:
            state.nameFilter = action.data
            break;
        case FILTER_PRODUCT_BY_MIN_PRICE:
            state.minPriceFilter = action.data
            break;
        case FILTER_PRODUCT_BY_MAX_PRICE:
            state.maxPriceFilter = action.data
            break;
        case FILTER_PRODUCT_BY_CATEGORY:
            state.categoryFilter = action.data
            break;
        default:
            break;
    }
    return { ...state };
}

export default filterReducer;