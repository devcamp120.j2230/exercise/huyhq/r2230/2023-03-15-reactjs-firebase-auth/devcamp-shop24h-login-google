import { combineReducers } from "redux";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import cartReducer from "../reducers/cart.reducer";
import filterReducer from "../reducers/filter.reducer";
import productReducer from "../reducers/product.reducer";
import userReducer from "../reducers/user.reducer";

//root chứa các task reducer 
const rootReducer = combineReducers({
    //gọi các task
    userReducer,
    productReducer,
    filterReducer,
    cartReducer
});

//tạo store
const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;